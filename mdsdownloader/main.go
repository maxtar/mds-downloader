package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"mime"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"time"
)

const (
	path    = "." //Set correct work directory
	siteurl = "http://mds-online.ru/source/"
)

func main() {
	erLogFile, err := os.Create(filepath.Join(path, "errors.txt"))
	if err != nil {
		log.Fatalf("Error creating error file: %v.", err)
	}
	defer erLogFile.Close()
	errors := log.New(erLogFile, "", 0)
	r, err := os.Open(filepath.Join(path, "parsed2.txt"))
	if err != nil {
		log.Fatalf("Error open src file: %v.", err)
	}
	defer r.Close()
	sc := bufio.NewScanner(r)
	var totalSize int64
	for sc.Scan() {
		name := sc.Text()
		fullURL := siteurl + url.PathEscape("/mds/"+name+".mp3")
		log.Printf("Downloading %q...", name)
		filesize, err := downloadFile(fullURL)
		if err != nil {
			errors.Println(name)
			log.Printf("Error download %q: %v", name, err)
			continue
		}
		totalSize += filesize
	}
	log.Printf("Total size of downloaded files: %d.", totalSize)
}

func downloadFile(URL string) (int64, error) {
	start := time.Now()
	resp, err := http.Get(URL)
	if err != nil {
		return 0, fmt.Errorf("error get resource: %v", err)
	}
	if resp.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("error download file with code: %d: %s", resp.StatusCode, resp.Status)
	}

	// Get real filename from server.
	_, dispParams, err := mime.ParseMediaType(resp.Header.Get("Content-Disposition"))
	if err != nil {
		return 0, fmt.Errorf("error get filename: %v", err)
	}
	body := resp.Body
	defer body.Close()
	realFileName := dispParams["filename"]
	dstName := filepath.Join(path, realFileName)
	dst, err := os.Create(dstName)
	defer dst.Close()
	if err != nil {
		log.Fatalf("error create file %q: %v.", dstName, err)
	}
	size, err := io.Copy(dst, body)

	if err != nil {
		log.Fatalf("Error download file: %v.", err)
	}
	log.Printf("File saved as %q. Elapsed time: %v. Size: %d.", realFileName, time.Since(start), size)
	return size, nil
}
