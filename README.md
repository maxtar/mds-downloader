# mds-downloader

Project contains utilities for download MDS files from <http://mds-online.ru>.

## parserss

Tool read rss.xml and get from it titles of stories. Result saving to file parsed.txt

## mdsdownloader

Tool read parsed.xml and trying download file with story from <http://mds-online.ru> site.
Story titles which couldn't download saving to different file for further processing.
