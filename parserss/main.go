package main

import (
	"bufio"
	"encoding/xml"
	"fmt"
	"log"
	"os"
)

// Rss present rss.xml file.
type Rss struct {
	XMLName xml.Name `xml:"rss"`
	Titles  []string `xml:"channel>item>title"`
}

func main() {
	f, err := os.Open("rss.xml")
	if err != nil {
		log.Fatalf("Couldn't read file: %v", err)
	}
	defer f.Close()

	dec := xml.NewDecoder(f)
	var rss Rss
	err = dec.Decode(&rss)
	if err != nil {
		log.Fatalf("error parse rss: %v\n", err)
	}
	out, err := os.Create("parsed.txt")
	if err != nil {
		log.Fatalf("couldn't crate output file: %v", err)
	}
	defer out.Close()
	w := bufio.NewWriter(out)

	for i := len(rss.Titles) - 1; i >= 0; i-- {
		fmt.Fprintf(w, "%s\n", rss.Titles[i])
		w.Flush()
	}
}
